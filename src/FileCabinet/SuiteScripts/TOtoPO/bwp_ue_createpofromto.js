var RECORDMODULE, SEARCHMODULE, TRANSACTIONMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'N/transaction'], runUserEvent);

function runUserEvent(record, search, transaction) {
	RECORDMODULE= record;
	SEARCHMODULE= search;
	TRANSACTIONMODULE= transaction;
	
	var returnObj = {};
//	returnObj['beforeLoad'] = _beforeLoad;
//	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');
	
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	
//	logRecord('scriptContext', scriptContext);
	
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {
	log.debug('afterSubmit started');
	
//	logRecord('scriptContext', scriptContext);
	var toRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.INTER_COMPANY_TRANSFER_ORDER,
		id: scriptContext.newRecord.id,
		isDynamic: false
	});
//	logRecord('toRecord', toRecord);
	var subsidiary = toRecord.getValue({ fieldId: 'subsidiary' });
	var toSubsidiary = toRecord.getValue({ fieldId: 'tosubsidiary' });
	var location = toRecord.getValue({ fieldId: 'transferlocation' });
	var lineCount = toRecord.getLineCount({ sublistId: 'item' });
	var itemLines = [];
	for (var i = 0 ; i < lineCount ; i++) {
		itemLines.push({
				item: toRecord.getSublistValue({ sublistId: 'item', fieldId: 'item', line: i }),
				quantity: toRecord.getSublistValue({ sublistId: 'item', fieldId: 'quantity', line: i }),
				expectedDate: toRecord.getSublistValue({ sublistId: 'item', fieldId: 'expectedreceiptdate', line: i }),
		});
	}

	var subsidiaryFieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.SUBSIDIARY,
		id: subsidiary,
		columns: ['representingvendor']
	});
	var entity;
	if ('representingvendor' in subsidiaryFieldsValues && subsidiaryFieldsValues.representingvendor.length > 0) {
		entity = subsidiaryFieldsValues.representingvendor[0].value;
	}
	
	// Create the purchase order record
	var poRecord = RECORDMODULE.create({
		type: RECORDMODULE.Type.PURCHASE_ORDER,
		isDynamic: true
	});

	// Read intitial currency - before setting value of entity (should be US dollar according to main subsidiary setup)
	var currency = poRecord.getValue({ fieldId: 'currency' });
	
	poRecord.setValue({
		fieldId: 'entity',
		value: entity
	});

	// Reset value of currency to its initial value - it may change after entity is set
	poRecord.setValue({
		fieldId: 'currency',
		value: currency
	});

	poRecord.setValue({
		fieldId: 'subsidiary',
		value: toSubsidiary
	});
	poRecord.setValue({
		fieldId: 'location',
		value: location
	});
	
	itemLines.forEach(function(itemLine) {
		poRecord.selectNewLine({
			sublistId: 'item'
		});
		poRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'item',
			value: itemLine.item
		});
		poRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'quantity',
			value: itemLine.quantity
		});
		poRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'expectedreceiptdate',
			value: itemLine.expectedDate
		});
		poRecord.commitLine({ sublistId: 'item' });
	});
	
	var internalId = poRecord.save();
	log.debug({
		title: 'PO is created',
		details: internalId
	});

	var poFieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.PURCHASE_ORDER,
		id: internalId,
		columns: ['tranid']
	});
	var tranid;
	if ('tranid' in poFieldsValues) {
		tranid = poFieldsValues.tranid;
	}
	if (tranid) {
		toRecord.setValue({
			fieldId: 'memo',
			value: 'Replaced by ' + tranid
		});
		toRecord.save();		
	}
	
	TRANSACTIONMODULE.void({
		id: toRecord.id,
		type: TRANSACTIONMODULE.Type.TRANSFER_ORDER
	});
	log.debug({
		title: 'TO is void',
		details: toRecord.id
	});
		
	log.debug('afterSubmit done');
}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}